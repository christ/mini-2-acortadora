from django.shortcuts import render
from .models import Contenido
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader


@csrf_exempt
def index(request):

    if request.method == "POST":

        url = request.POST['url']
        shorted = request.POST['shorted']
        if not url.startswith('http://') and not url.startswith('https://'):
            url = "https://" + url
        try:

            c = Contenido.objects.get(shorted=shorted)
            c.delete()
        except Contenido.DoesNotExist:
            pass

        c = Contenido(shorted=shorted, url=url)
        c.save()


    content_list = Contenido.objects.all()
    template = loader.get_template('acorta/index.html')
    context = {'content_list': content_list}

    return HttpResponse(template.render(context, request))


def get_content(request, shorted):
    clave = shorted
    try:
        contenido = Contenido.objects.get(shorted=clave)

        template = loader.get_template('acorta/redirect.html')
        context = {'content': contenido}

        return HttpResponse(template.render(context, request))
    except Contenido.DoesNotExist:
        return HttpResponse("La url acortada introducida -> " + shorted + " no está en la base de datos", status=404)

